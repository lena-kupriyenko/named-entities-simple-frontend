//then find these places in text
//highlight them

//function for reading text files
function readTextFile(file) {
    var rawFile = new XMLHttpRequest();
    var allText;
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function() {
        if(rawFile.readyState === 4)
            {
                if(rawFile.status === 200 || rawFile.status == 0) 
                    {
                        allText = rawFile.responseText;
                    }
            }
    }
    rawFile.send(null);
    return allText;
}
//parses given json file and returns matches 
function parseJsonForMatches(file) {
    var data = readTextFile(file);
    data = JSON.parse(data);
    //all matches needed for highlighting are stored in 
    //the variable matches
    var matches = data._source.matches;
    return matches;
}

//parses given json file and returns algorithm id
function parseJsonForAlgorithmId(file) {
    var data = readTextFile(file);
    data = JSON.parse(data);
    //all matches needed for highlighting are stored in 
    //the variable matches
    var algorithmId = data._source.algorithmId;
    return algorithmId;
}
//returns the array of the start positions of text A
function extractStartPosA(file) {
    var parsed = parseJsonForMatches(file);
    var extractedPos = [];
        //console.log(parsed);
    for(var i=0; i < parsed.length; i++) {
        var arrayTemp = parsed[i];
        extractedPos.push(arrayTemp[0]);
    }
    return extractedPos;
}

//returns the array of the end positions of text A
function extractEndPosA(file) {
    var parsed = parseJsonForMatches(file);
    var extractedPos = [];

    for(var i=0; i < parsed.length; i++) {
        var arrayTemp = parsed[i];
        extractedPos.push(arrayTemp[1]);
    }
    
    return extractedPos;
}
//returns the array of the start positions of text B
function extractStartPosB(file){
    var parsed = parseJsonForMatches(file);
    var extractedPos = [];
    for(var i=0; i < parsed.length; i++) {
        var arrayTemp = parsed[i];
        extractedPos.push(arrayTemp[2]);
    }
    
    return extractedPos;
}

//returns the array of the end positions of text B
function extractEndPosB(file) {
    var parsed = parseJsonForMatches(file);
    var extractedPos = [];
        //console.log(parsed);
    for(var i=0; i < parsed.length; i++) {
        var arrayTemp = parsed[i];
        extractedPos.push(arrayTemp[3]);
    }
    
    return extractedPos;
}

//finds all named entities in the text at given positions
function findNamedEntity(startPos, endPos, file) {
    var inputText = readTextFile(file);
    var namedEntity = inputText.substring(startPos-1, endPos-1);
    console.log(namedEntity);
}

//highlights all named entities in two given texts according to the file selected
//function highlightAll (fileA) {
//    inputText = document.getElementById("TextA");
//    var wholeText = inputText.innerText;//just the text without tags!
//    var startPositionsA = extractStartPosA('test_first.txt');
//    var endPositionsA = extractEndPosA('test_first.txt');
//    //at this moment highlights only the first named entity
//    //highlight all named entities in the first text
//        //var indexOne = startPositionsA[i]-1;
//        //var indexTwo = endPositionsA[i]-1;
//        var innerHTML = inputText.innerHTML;
//        //innerHTML = wholeText.substring(0, startPositionsA[i]-1) + "<span class='namedEntity'>" + innerHTML.substring(startPositionsA[i]-1, en/dPositionsA[i]-1) +
//        //"</span>" + innerHTML.substring(startPositionsA[i]-1 + ((endPositionsA[i]-1)-(startPositionsA[i]-1));
//        //inputText.innerHTML = innerHTML;
//    for(var i=0; i < startPositionsA.length; i++) {
//        var inHtml =  wholeText.substring(0, startPositionsA[i]-1) + "<span class='namedEntity'>" + wholeText.substring(startPositionsA[i]-1, endPositionsA[i]-1) + "</span> " + wholeText.substring(endPositionsA[i],wholeText.length);
//        
//        console.log(inHtml);
//    }
//        document.getElementById("TextA").innerHTML = inHtml;
//    console.log(inHtml);
//}

function highlightAll(fileA) {
    //get out all named entities
    $('div').highlight('one');
}
//just for testing at this moment!
function test() {
 var data = readTextFile("source-document04337.txt");
 //var jsonfile = readTextFile("test_first.txt");
 //console.log(jsonfile);
 parseJsonForMatches("test_first.txt");
    document.getElementById('TextA').innerText=readTextFile("source-document04337.txt");
    document.getElementById('TextB').innerText=readTextFile("selected_source-document04249.txt");
    //named entities in the text A and in the text B should be of the same
    // color
    
    //findNamedEntity(startPositionsA[1], endPositionsA[1], "source-document04337.txt");
    //findNamedEntity(startPositionsB[1],endPositionsB[1],"selected_source-document04249.txt");
    
    //highlightAll("source-document04337.txt");
}
